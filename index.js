const token = process.env.TOKEN; //TODO: config.json?

const Discord = require("discord.js"),
  client = new Discord.Client({ autoReconnect: true });
const moment = require("moment"); //TODO: library is fat, pure JS replacement

function sessionCountdown() {
  let zulu1 = moment.utc(),
    diff = zulu1.date() - zulu1.day();

  if (zulu1.day() !== 0) {
    diff += +6;
  }

  zulu1.date(diff);
  zulu1.hours(18);
  zulu1.minutes(0);
  zulu1.seconds(0);
  zulu1.milliseconds(0);

  setInterval(() => {
    let local2 = new Date().getTime(),
      off2 = new Date().getTimezoneOffset(),
      zulu2 = new Date(local2 + off2),
      distance = zulu1 - zulu2;

    let days = Math.floor(distance / (1000 * 60 * 60 * 24)),
      hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
      minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)),
      seconds = Math.floor((distance % (1000 * 60)) / 1000);

    if (distance < 0) {
      client.user.setActivity("EXPIRED");
    } else {
      client.user.setActivity(
        "in " + days + "D " + hours + "H " + minutes + "M " + seconds + "S"
      );
    }
  }, 30000);
}

function clearChannel(message) {
  message.channel.fetchMessages().then(
    function(list) {
      message.channel.bulkDelete(list);
    },
    function(err) {
      message.channel.send("ERROR: ERROR CLEARING CHANNEL.");
    }
  );
}

client.on("ready", () => {
  sessionCountdown();
});

client.on("message", message => {
  if (
    message.content === "!purge" &&
    message.member.hasPermission("MANAGE_MESSAGES")
  ) {
    clearChannel(message);
  }
});

client.login(token);
